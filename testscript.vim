" (This script applies on yml files)
"
" GOAL OF THE SCRIPT :
" ====================
" The goal is to give a complete key sequence of a line
"
" Example file:
" https://github.com/Halleck45/BehatWizardBundle/blob/master/Resources/translations/messages.en.yml
"
" If I am line 48 on "subtitle:" the script should give me: "hbw > edit > main > subtitle"
"
"
" WORKFLOW
" ========
"
" 1/ Find the indentation steps (2 chars? 4? …)
"       - Just find the first non empty line starting with spaces and count them
"
" 2/ Take the current line (if it is not empty) and retrieve the key alone
"
" 3/ Loop on the parents (loop on the previous lines with smaller indentation
" until we reach an indentation == 0)
"
" 4/ Store every key in a list
"
" 5/ Echo the list in reverse order like "Root > Parent > Child > Current Line"




function! GetIndentationStep()
    call cursor(1,1)
    call search('^\s')
    return indent(".")
endfunction


function! AddParentsKeys()
    let goToLineCmd = ':' . s:inputLineNumber
    :exec goToLineCmd

    let l:parentIndent = s:currentIndent - s:indentationStep
    while l:parentIndent >= 0

        if l:parentIndent == 0
            :call search('^\S', 'b')
        else
            let searchCmd = ":call search('^\\s\\{" . l:parentIndent . "}\\S', 'b')"
            :exec searchCmd
        endif

        let parentKey = matchstr(getline("."), '\s*\zs.\+\ze:')
        :call add(s:keys, parentKey)
        let s:currentIndent = indent(".")
        if l:parentIndent >= 0
            let l:parentIndent = s:currentIndent - s:indentationStep
        endif
    endwhile
endfunction


function! GetKeySequence(inputLineNumber)
    let s:inputLineNumber = a:inputLineNumber
    let currentLine = getline(s:inputLineNumber)
    let currentKey = matchstr(currentLine, '\s*\zs.\+\ze:')
    let s:keys = [currentKey]

    if !empty(currentLine)
        let s:indentationStep = GetIndentationStep()
        let s:currentIndent = indent(s:inputLineNumber)

        :call AddParentsKeys()
        :call reverse(s:keys)
        echo join(s:keys, " > ")

        let goBackToLineCmd = ':'.s:inputLineNumber
        :exec goBackToLineCmd
    endif
endfunction


nmap ,ind :call GetKeySequence(line("."))<CR>
